from django.urls import path
from .views import *

urlpatterns = {
    path('lista/',client_list, name ='client_list'),
    path('new/',cliente_new,name='client_new'),
    path ('update/<int:id>/', client_update, name = 'client_update'),
    path ('delete/<int:id>/', client_delete, name = 'client_delete')
}