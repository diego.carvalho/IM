from django.db import models

# Create your models here.
class Client (models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    age = models.IntegerField()
    salary = models.DecimalField(decimal_places=2,max_digits=5)
    bio = models.TextField()

    def __str__(self):
        return self.first_name + ' '+ self.last_name